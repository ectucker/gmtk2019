part of gmtk;

class PixelGame extends BaseGame {

  Vector4 color = Colors.white;

  double size = 0.7;

  SpriteBatch draw;
  Camera2D screen;
  Texture pixel;
  int renderSize;

  PixelController controller;

  @override
  create() {
    draw = SpriteBatch.defaultShader(gl, maxSprites: 5);
    pixel = assetManager.get("white");
    resize(width, height);

    correct = List.generate(8, (index) => assetManager.get("Correct${index + 1}"));
    levelUp = assetManager.get("LevelUp");
    wrong = assetManager.get("Wrong");
    victory = assetManager.get("Victory");

    controller = PixelController(this);
  }

  @override
  update(num delta) {
    controller.update(delta);
  }

  @override
  preload() {
    assetManager.load("white", loadTexture(gl, "img/white.png", nearest));
    assetManager.load("Correct1", loadSound(audio, "snd/Correct1.wav"));
    assetManager.load("Correct2", loadSound(audio, "snd/Correct2.wav"));
    assetManager.load("Correct3", loadSound(audio, "snd/Correct3.wav"));
    assetManager.load("Correct4", loadSound(audio, "snd/Correct4.wav"));
    assetManager.load("Correct5", loadSound(audio, "snd/Correct5.wav"));
    assetManager.load("Correct6", loadSound(audio, "snd/Correct6.wav"));
    assetManager.load("Correct7", loadSound(audio, "snd/Correct7.wav"));
    assetManager.load("Correct8", loadSound(audio, "snd/Correct8.wav"));
    assetManager.load("LevelUp", loadSound(audio, "snd/LevelUp.wav"));
    assetManager.load("Wrong", loadSound(audio, "snd/Wrong.wav"));
    assetManager.load("Victory", loadSound(audio, "snd/Victory.wav"));
  }

  @override
  render(num delta) {
    gl.clearScreen(Colors.black);

    renderSize = (min(width, height) * size).floor();

    draw.projection = screen.combined;
    draw.color = color;
    draw.begin();
    draw.draw(pixel, -renderSize / 2, -renderSize / 2, width: renderSize, height: renderSize);
    draw.end();
  }

  @override
  config() {
    scaleMode = ScaleMode.resize;
  }

  @override
  resize(num width, num height) {
    screen = Camera2D.originCenter(width, height);
    renderSize = (min(width, height) * size).floor();
  }

}
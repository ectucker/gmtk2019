part of gmtk;

final Map<int, int> keys = {
  -1: -1,
  0: KeyCode.R,
  1: KeyCode.G,
  2: KeyCode.B,
  3: KeyCode.C,
  4: KeyCode.M,
  5: KeyCode.Y
};

final Map<int, Vector4> colors = {
  -1: Vector4(0.2, 0.2, 0.2, 1.0),
  0: Colors.red,
  1: Colors.green,
  2: Colors.blue,
  3: Colors.cyan,
  4: Colors.magenta,
  5: Colors.yellow
};

Vector4 hexColor(String hex) {
  Vector4 color = Vector4.zero();
  Colors.fromHexString(hex, color);
  return color;
}

final Map<int, List<Vector4>> colorShades = {
  -1: [Vector4(0.2, 0.2, 0.2, 1.0)],
  0: [
    hexColor("FE8484"),
    hexColor("530000"),
    hexColor("FE2020"),
    hexColor("BA0000"),
    hexColor("FE5C5C"),
    hexColor("DB0000"),
    hexColor("6B0000"),
    hexColor("FE3939"),
    hexColor("FE4D4D")
  ],
  1: [
    hexColor("9CFF88"),
    hexColor("00BB27"),
    hexColor("05E177"),
    hexColor("30AD23"),
    hexColor("7DF481"),
    hexColor("75DB1B"),
    hexColor("22EE5B"),
    hexColor("11772D"),
    hexColor("3ED715")
  ],
  2: [
    hexColor("88AAFF"),
    hexColor("1100BB"),
    hexColor("2277AA"),
    hexColor("4422EE"),
    hexColor("7788EE"),
    hexColor("0033EE"),
    hexColor("221177"),
    hexColor("6666FF"),
    hexColor("5588EE")
  ],
  3: [
    hexColor("1A8180"),
    hexColor("00FDFF"),
    hexColor("35E0FF"),
    hexColor("009CB8"),
    hexColor("4ED6CB"),
    hexColor("00FFCC"),
    hexColor("007C97"),
    hexColor("AEFFFF"),
    hexColor("25BDB1")
  ],
  4: [
    hexColor("811A74"),
    hexColor("FE8CFF"),
    hexColor("FF35C5"),
    hexColor("B80083"),
    hexColor("D64ECF"),
    hexColor("FF00FF"),
    hexColor("970067"),
    hexColor("FFAEF4"),
    hexColor("BD25B4")
  ],
  5: [
    hexColor("EF9B0F"),
    hexColor("FFD800"),
    hexColor("FFEE75"),
    hexColor("FFBA00"),
    hexColor("FFFF00"),
    hexColor("FFDF00"),
    hexColor("FFF77D"),
    hexColor("FFFACD"),
    hexColor("FFBF00")
  ],
};

List<Sound> correct;
Sound levelUp;
Sound wrong;
Sound victory;

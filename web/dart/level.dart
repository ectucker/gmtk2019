part of gmtk;

abstract class Stage {

  Random rand;

  int colorStart;
  int colorEnd;

  bool complete;

  int lastRand = -1;
  
  bool varyColors;

  int diffRand(int max) {
    int next = rand.nextInt(max);
    if(next != lastRand) {
      lastRand = next;
      return next;
    }
    return diffRand(max);
  }
  
  getColor(int color) {
    if(varyColors) {
      return colorShades[color][rand.nextInt(colorShades[color].length)];
    } else {
      return colors[color];
    }
  }

}

class ReactionStage extends Stage {

  Random rand;

  int colorStart;
  int colorEnd;

  double time;

  Vector4 currentColor;
  int currentKey;

  double countdown = -100;

  int combo = 1;

  bool locked = false;
  
  bool varyColors;

  ReactionStage(this.colorStart, this.colorEnd, this.time, this.varyColors, this.rand);

  void pickColor() {
    int color = colorStart + diffRand(colorEnd - colorStart);
    currentKey = keys[color];
    currentColor = getColor(color);
    countdown = time;
    locked = false;
  }
}

class MemorizationStage extends Stage {

  Random rand;

  int colorStart;
  int colorEnd;

  int patternLength;
  double colorTime;

  List<Vector4> patternColors = [];
  List<int> patternKeys = [];

  int showIndex;
  double showTime;

  int playIndex;
  double playTime;

  int patternsFinished = 0;

  bool locked = false;
  
  bool varyColors;

  MemorizationStage(this.colorStart, this.colorEnd, this.patternLength, this.colorTime, this.varyColors, this.rand) {
    showIndex = -1;
  }

  void pickPattern() {
    patternColors.clear();
    patternKeys.clear();
    for(int i = 0; i< patternLength; i++) {
      int color = colorStart + diffRand(colorEnd - colorStart);
      patternKeys.add(keys[color]);
      patternColors.add(getColor(color));
    }

    showIndex = 0;
    showTime = colorTime;

    playIndex = -1;
    playTime = colorTime * patternLength;

    locked = false;
  }

}
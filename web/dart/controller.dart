part of gmtk;

class PixelController {
  PixelGame pixel;

  Keyboard keyboard;

  TweenManager tweenManager;

  Random rand;

  bool started = false;
  bool ended = false;

  List<Stage> stages;
  int currentStage = 0;
  Stage get stage => stages[currentStage];

  int breatheDir = -1;

  PixelController(this.pixel) {
    keyboard = pixel.keyboard;
    tweenManager = pixel.tweenManager;

    rand = Random();
    stages = [
      ReactionStage(0, 3, 2.0, false, rand),
      ReactionStage(-1, 3, 2.0, false, rand),
      MemorizationStage(0, 3, 3, 1.0, false, rand),
      ReactionStage(0, 3, 2.0, true, rand),
      ReactionStage(3, 6, 2.0, false, rand),
      MemorizationStage(0, 6, 3, 1.0, false, rand),
      ReactionStage(0, 6, 1.5, false, rand),
      ReactionStage(-1, 6, 1.5, true, rand),
      MemorizationStage(0, 6, 5, 1.0, false, rand),
      ReactionStage(0, 6, 1.25, false, rand),
      ReactionStage(-1, 6, 1.25, true, rand),
      MemorizationStage(0, 6, 5, 0.75, true, rand)
    ];
  }

  update(double delta) {
    if (pixel.size > 0.7) {
      pixel.size -= delta;
    }
    if (pixel.size < 0.69) {
      pixel.size += delta;
      if (pixel.size > 0.7) {
        pixel.size = 0.7;
      }
    }

    if (!ended) {
      if (!started && currentStage < stages.length) {
        breathePixel(delta * 0.2);
        if (pixel.keyboard.keyPressed(KeyCode.W)) {
          started = true;
        }
      }

      if (currentStage == stages.length) {
        ended = true;
        fadeInVictory();
      }

      if (started) {
        if (stage is ReactionStage) {
          ReactionStage rs = stage;

          // First frame
          if (rs.countdown < -10) {
            rs.pickColor();
            fadeIn(rs.currentColor);
          }

          rs.countdown -= delta;
          if (rs.countdown <= 0) {
            if (rs.currentKey == -1 && !rs.locked) {
              inputCorrect(rs);
            } else if (!rs.locked) {
              inputWrong(rs);
              rs.pickColor();
              fadeIn(rs.currentColor);
            } else {
              rs.pickColor();
              fadeIn(rs.currentColor);
            }
          } else if (pixel.keyboard.keyJustPressed(rs.currentKey) &&
              !rs.locked) {
            inputCorrect(rs);
          } else if ((anyColorJustDown() || (keyboard.keyJustPressed(KeyCode.W) && rs.time - rs.countdown > 0.1)) && !rs.locked) {
            inputWrong(rs);
          }
        } else if (stage is MemorizationStage) {
          MemorizationStage ms = stage;

          if (ms.showIndex < ms.patternLength) {
            if (ms.showIndex == -1) {
              ms.pickPattern();
              fadeIn(ms.patternColors[ms.showIndex]);
              correct[ms.showIndex].play();
              pixel.size = 0.71;
            }

            ms.showTime -= delta;

            if (ms.showTime < 0) {
              ms.showIndex += 1;
              ms.showTime = ms.colorTime;
              if (ms.showIndex == ms.patternLength) {
                fadeIn(Colors.black);
                ms.playIndex = 0;
              } else {
                fadeIn(ms.patternColors[ms.showIndex]);
                correct[ms.showIndex].play();
              }
              pixel.size = 0.71;
            }
          } else {
            ms.playTime -= delta;

            if (ms.playIndex == ms.patternLength && !ms.locked) {
              ms.patternsFinished++;
              ms.locked = true;
              ms.playTime = 0.8;
            } else if (!ms.locked &&
                pixel.keyboard.keyJustPressed(ms.patternKeys[ms.playIndex])) {
              correct[ms.playIndex].play();
              fadeIn(ms.patternColors[ms.playIndex]);
              ms.playIndex++;
              pixel.size = 0.71;
            } else if (anyColorJustDown() && !ms.locked) {
              ms.locked = true;
              wrong.play();
              ms.playTime = 0.5;
              fadeIn(Colors.black);
            }

            if (ms.playIndex == ms.patternLength && ms.playTime < 0.4 && ms.playTime > 0.3) {
              fadeIn(Colors.black);
              ms.playTime = 0.3;
            }
            if (ms.playTime < 0) {
              if (!ms.locked) {
                wrong.play();
              }
              ms.showIndex = -1;
              if(ms.patternsFinished >= 3) {
                levelUp.play();
                pixel.size = 0.2;
                currentStage++;
                started = false;
                fadeIn(Colors.white);
              }
            }
          }
        }
      }
    }
  }

  void inputCorrect(ReactionStage rs) {
    if (rs.combo == 8) {
      levelUp.play();
      pixel.size = 0.2;
      currentStage++;
      started = false;
      fadeIn(Colors.white);
    } else {
      correct[rs.combo].play();
      rs.combo += 1;
      rs.pickColor();
      fadeIn(rs.currentColor);
      pixel.size = 0.71;
    }
  }

  void inputWrong(ReactionStage rs) {
    rs.locked = true;
    rs.combo = 1;
    wrong.play();
    pixel.size = 0.75;
  }

  bool anyColorJustDown() {
    for (int i = 0; i < 6; i++) {
      if (keyboard.keyJustPressed(keys[i])) {
        return true;
      }
    }
    return false;
  }

  List<double> vectorList(Vector4 target) {
    return [target.r, target.g, target.b, target.a];
  }

  List<Function> vectorGetters(Vector4 get) {
    return [() => get.r, () => get.g, () => get.b, () => get.a];
  }

  List<Function> vectorSetters(Vector4 set) {
    return [
      (v) => set.r = v,
      (v) => set.g = v,
      (v) => set.b = v,
      (v) => set.a = v
    ];
  }

  fadeIn(Vector4 color, [double delay = 0.0]) {
    Tween()
      ..target = vectorList(color)
      ..get = vectorGetters(pixel.color)
      ..set = vectorSetters(pixel.color)
      ..duration = 0.2
      ..delay = 0.0
      ..start(tweenManager);
  }

  fadeInVictory() {
    pixel.color.setFrom(Colors.white);
    Tween()
      ..target = vectorList(hexColor("D4AF37"))
      ..get = vectorGetters(pixel.color)
      ..set = vectorSetters(pixel.color)
      ..duration = 0.5
      ..callback = () {
        victory.play();
      }
      ..start(tweenManager);
  }

  breathePixel(delta) {
    pixel.color.r += delta * breatheDir;
    pixel.color.g += delta * breatheDir;
    pixel.color.b += delta * breatheDir;
    if (pixel.color.r >= 1.0) {
      pixel.color.r = 1.0;
      pixel.color.g = 1.0;
      pixel.color.b = 1.0;
      breatheDir = -1;
    }
    if (pixel.color.r <= 0.7) {
      breatheDir = 1;
    }
  }
}

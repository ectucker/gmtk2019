library gmtk;

import 'package:cobblestone/cobblestone.dart';

part 'dart/game.dart';
part 'dart/controller.dart';
part 'dart/things.dart';
part 'dart/level.dart';

void main() {
  PixelGame();
}
